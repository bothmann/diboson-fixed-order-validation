# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/ZeeZmm_dR
Rebin=5
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/4l_m
Title=Four-lepton invariant mass
XLabel=$m_{\text{2e2}\mu}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\text{2e2}\mu}$ [fb/GeV]
LogX=1
Rebin=2
XMin=70
XMax=5000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/4l_m_log
LegendXPos=0.2
LegendYPos=0.6
YLabelSep=7.5
LeftMargin=1.6
Title=Four-lepton invariant mass
XLabel=$m_{\text{2e2}\mu}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\text{2e2}\mu}$ [fb/GeV]
LogX=1
XMin=70
XMax=2511.887
RatioPlotYMin=0.5001
RatioPlotYMax=1.4999
RatioPlotYMajorTickMarks=20
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/4l_halved_m
Title=Four-lepton invariant mass
XLabel=$m_{\text{2e2}\mu}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\text{2e2}\mu}$ [fb/GeV]
XMin=70
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/4l_m_low
Title=Four-lepton invariant mass
XLabel=$m_{\text{2e2}\mu}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\text{2e2}\mu}$ [fb/GeV]
XMin=70
XMax=500
LegendXPos=0.65
LegendYPos=0.6
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/Zee_m
Title=$e^+e^-$ invariant mass
XLabel=$m_{\text{e}^+\text{e}^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\text{e}^+\text{e}^-}$ [fb/GeV]
Rebin=2
XMin=70
XMax=500
LegendXPos=0.65
LegendYPos=0.6
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/Zmm_m
Title=$\mu^+\mu^-$ invariant mass
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+\mu^-}$ [fb/GeV]
Rebin=2
XMin=70
XMax=500
LegendXPos=0.65
LegendYPos=0.6
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/ZeeZmm_dphi
Title=Z-boson distance
XLabel=$\Delta \phi_{2e,2\mu}$
YLabel=$\text{d}\sigma/\text{d}\Delta \phi_{\text{2e2}\mu}$ [fb]
LogX=0
LegendXPos=0.25
LegendYPos=0.9
Scale=1e3
# END PLOT


# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/4l_j_dphi
Title=Four-lepton vs. leading jet distance
XLabel=$\Delta \phi_{4\ell,j_1}$ [$^\circ$]
YLabel=$\text{d}\sigma/\text{d}\Delta \phi_{4\ell,j_1}$ [fb/$^\circ$]
LogX=0
LegendXPos=0.1
LegendYPos=0.9
YMax=1e0
YMin=2e-5
RatioPlotYMin=0.5001
RatioPlotYMax=1.4999
RatioPlotYMajorTickMarks=20
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/ZeeZmm_dy
Title=Z-boson rapidity distance
XLabel=$\Delta y_{2e,2\mu}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}\Delta y_{\text{2e2}\mu}$ [fb/GeV]
LogX=0
XMax=5
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/Zee_pT
YLabelSep=7.5
LeftMargin=1.6
Title=$\text{e}^+\text{e}^-$ transverse momentum
XLabel=$p_{\mathrm{T},2e}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{{\text{T}},{2e}}$ [fb/GeV]
Rebin=10
LogX=0
XMin=0
XMax=1000
YMin=1e-6
YMax=3e-1
XCustomMajorTicks=100 100 200 200 300 300 400 400 500 500 600 600 700 700 800 800 900 900 1000 1000
XCustomMinorTicks=50 150 250 350 450 550 650 750 850 950
LegendXPos=0.45
LegendYPos=0.95
RatioPlotYMin=0.5001
RatioPlotYMax=1.4999
RatioPlotYMajorTickMarks=20
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/Zee_pT_log
LegendXPos=0.2
LegendYPos=0.65
YLabelSep=7.5
LeftMargin=1.6
Title=$e^+e^-$ transverse momentum
XLabel=$p_{\mathrm{T},2{\text{e}}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,{2e}}$ [fb/GeV]
LogX=1
Rebin=1
XMax=1000
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/Zmm_pT
Title=$\mu^+\mu^-$ transverse momentum
XLabel=$p_{T,2\mu}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,2\mu}$ [fb/GeV]
LogX=0
Rebin=2
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/Zmm_pT_log
Title=$\mu^+\mu^-$ transverse momentum
XLabel=$p_{T,2\mu}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,2\mu}$ [fb/GeV]
LogX=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/ZeeZmm_pT_log
Title=$e^+e^-$ transverse momentum
XLabel=$p_{T,{2e}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,{2e}}$ [fb/GeV]
LogX=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_dR
Title=$\Delta R_{\mu^+e^-}$
XLabel=$\Delta R_{\mu^+e^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{\mu^+e^-}$ [fb]
LogX=0
XMax=5
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_y
Title=${\mu^+e^-}$ rapidity
XLabel=$y_{\mu^+e^-}$
YLabel=$\text{d}\sigma/\text{d}y_{\mu^+e^-}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
YMin=0
YMax=3
RatioPlotYMin=0.8001
RatioPlotYMax=1.1999
RatioPlotYMajorTickMarks=10
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_y
Title=${\mu^+e^+}$ rapidity
XLabel=$y_{\mu^+e^+}$
YLabel=$\text{d}\sigma/\text{d}y_{\mu^+e^+}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
RatioPlotYMin=0.5001
RatioPlotYMax=1.4999
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_y
Title=${e^+e^-}$ rapidity
XLabel=$y_{e^+e^-}$
YLabel=$\text{d}\sigma/\text{d}y_{e^+e^-}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_y
Title=${\mu^+\mu^-}$ rapidity
XLabel=$y_{\mu^+\mu^-}$
YLabel=$\text{d}\sigma/\text{d}y_{\mu^+\mu^-}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_y
Title=${\mu^-e^-}$ rapidity
XLabel=$y_{\mu^-e^-}$
YLabel=$\text{d}\sigma/\text{d}y_{\mu^-e^-}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_y
Title=${\mu^-e^+}$ rapidity
XLabel=$y_{\mu^-e^+}$
YLabel=$\text{d}\sigma/\text{d}y_{\mu^-e^+}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_y
Title=${\mu^+}$ rapidity
XLabel=$y_{\mu^+}$
YLabel=$\text{d}\sigma/\text{d}y_{\mu^+}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_y
Title=${\mu^-}$ rapidity
XLabel=$y_{\mu^-}$
YLabel=$\text{d}\sigma/\text{d}y_{\mu^-}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/positron_y
Title=${e^+}$ rapidity
XLabel=$y_{e^+}$
YLabel=$\text{d}\sigma/\text{d}y_{e^+}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/jet1_y
Title=leading-jet rapidity
XLabel=$y_{j_1}$
YLabel=$\text{d}\sigma/\text{d}y_{j_1}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/jet2_y
Title=second-jet rapidity
XLabel=$y_{j_2}$
YLabel=$\text{d}\sigma/\text{d}y_{j_2}$ [fb]
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/jet_multiplicity
Title=jet multiplicity (anti-$k_T$ $R=0.4$)
XLabel=$N_{\text{jet}}$
YLabel=$\text{d}\sigma/\text{d}N_{\text{jet}}$ [fb]
LogX=0
XMin=0
XMax=10
LegendXPos=0.75
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_dphi
Title=$\Delta \phi_{\mu^+e^-}$
XLabel=$\Delta \phi_{\mu^+e^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta \phi_{\mu^+e^-}$ [fb]
LogX=0
LogY=0
LegendXPos=0.25
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_dphi
Title=$\Delta \phi_{\mu^+e^+}$
XLabel=$\Delta \phi_{\mu^+e^+}$
YLabel=$\text{d}\sigma/\text{d}\Delta \phi_{\mu^+e^-}$ [fb]
LogX=0
LogY=0
LegendXPos=0.15
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_dphi
Title=$\Delta \phi_{e^+e^-}$
XLabel=$\Delta \phi_{e^+e^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta \phi_{e^+e^-}$ [fb]
LogX=0
LogY=0
LegendXPos=0.15
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_dphi
Title=$\Delta \phi_{\mu^+\mu^-}$
XLabel=$\Delta \phi_{\mu^+\mu^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta \phi_{\mu^+\mu^-}$ [fb]
LogX=0
LogY=0
LegendXPos=0.15
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_dphi
Title=$\Delta \phi_{\mu^-e^-}$
XLabel=$\Delta \phi_{\mu^-e^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta \phi_{\mu^-e^-}$ [fb]
LogX=0
LogY=0
LegendXPos=0.15
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_dphi
Title=$\Delta \phi_{\mu^-e^+}$
XLabel=$\Delta \phi_{\mu^-e^+}$
YLabel=$\text{d}\sigma/\text{d}\Delta \phi_{\mu^-e^+}$ [fb]
LogX=0
LogY=0
LegendXPos=0.15
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_dR
Title=$\Delta R_{\mu^-e^-}$
XLabel=$\Delta R_{\mu^+e^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{\mu^-e^-}$ [fb]
LogX=0
XMax=5
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_costheta
Title=$\cos\theta_{\mu^+e^-}$
XLabel=$\cos\theta_{\mu^+e^-}$
YLabel=$\text{d}\sigma/\text{d}\cos\theta_{\mu^+e^-}$ [fb]
LogY=0
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_costheta
Title=$\cos\theta_{\mu^-e^+}$
XLabel=$\cos\theta_{\mu^-e^+}$
YLabel=$\text{d}\sigma/\text{d}\cos\theta_{\mu^-e^+}$ [fb]
LogY=0
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_costheta
Title=$\cos\theta_{\mu^-e^-}$
XLabel=$\cos\theta_{\mu^-e^-}$
YLabel=$\text{d}\sigma/\text{d}\cos\theta_{\mu^-e^-}$ [fb]
LogY=0
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_costheta
Title=$\cos\theta_{e^+e^-}$
XLabel=$\cos\theta_{e^+e^-}$
YLabel=$\text{d}\sigma/\text{d}\cos\theta_{e^+e^-}$ [fb]
LogY=0
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_costheta
Title=$\cos\theta_{\mu^+\mu^-}$
XLabel=$\cos\theta_{\mu^+\mu^-}$
YLabel=$\text{d}\sigma/\text{d}\cos\theta_{\mu^+\mu^-}$ [fb]
LogY=0
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_costheta
Title=$\cos\theta_{\mu^+e^+}$
XLabel=$\cos\theta_{\mu^+e^+}$
YLabel=$\text{d}\sigma/\text{d}\cos\theta_{\mu^+e^+}$ [fb]
LogY=0
LegendXPos=0.15
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_dR
Title=$\Delta R_{\mu^+e^+}$
XLabel=$\Delta R_{\mu^+e^+}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{\mu^+e^+}$ [fb]
LogX=0
XMax=5
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_dR
Title=$\Delta R_{\mu^-e^+}$
XLabel=$\Delta R_{\mu^-e^+}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{\mu^-e^+}$ [fb]
LogX=0
XMax=5
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_dR
Title=$\Delta R_{\mu^+\mu^-}$
XLabel=$\Delta R_{\mu^+\mu^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{\mu^+\mu^-}$ [fb]
LogX=0
XMax=5
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_halved_m
Title=$\mu^+e^-$ invariant mass
XLabel=$m_{\mu^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+e^-}$ [fb/GeV]
XMin=0
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_halved_m
Title=$\mu^-e^+$ invariant mass
XLabel=$m_{\mu^-e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^-e^+}$ [fb/GeV]
XMin=0
XMax=2500
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_halved_m
Title=$\mu^-e^-$ invariant mass
XLabel=$m_{\mu^-e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^-e^-}$ [fb/GeV]
XMin=0
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_halved_m
Title=$e^+e^-$ invariant mass
XLabel=$m_{e^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{e^+e^-}$ [fb/GeV]
XMin=0
XMax=2500
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_halved_m
Title=$\mu^+e^+$ invariant mass
XLabel=$m_{\mu^+e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+e^+}$ [fb/GeV]
XMin=0
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_halved_m
Title=$\mu^+\mu^-$ invariant mass
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+\mu^-}$ [fb/GeV]
XMin=0
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_halved_pT
Title=$\mu^+\mu^-$ transverse momentum
XLabel=$p_{T,\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+\mu^-}$ [fb/GeV]
XMin=0
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_halved_pT
Title=$\mu^-e^+$ transverse momentum
XLabel=$p_{T,\mu^-e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^-e^+}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_halved_pT
Title=$\mu^-e^-$ transverse momentum
XLabel=$p_{T,\mu^-e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^-e^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_pT_log
Title=$\mu^+\mu^-$ transverse momentum
XLabel=$p_{T,\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+\mu^-}$ [fb/GeV]
XMin=25
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_pT
Title=$\mu^-e^-$ transverse momentum
XLabel=$p_{T,\mu^-e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^-e^-}$ [fb/GeV]
XMin=0
Rebin=2
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_pT_log
Title=$\mu^-e^-$ transverse momentum
XLabel=$p_{T,\mu^-e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^-e^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_pT
Title=$\mu^+e^+$ transverse momentum
XLabel=$p_{T,\mu^+e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+e^+}$ [fb/GeV]
Rebin=2
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_pT_log
Title=$\mu^+e^+$ transverse momentum
XLabel=$p_{T,\mu^+e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+e^+}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_m_log
Title=$\mu^+e^-$ invariant mass
XLabel=$m_{\mu^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+e^-}$ [fb/GeV]
XMin=0
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_m
Title=$\mu^+e^-$ invariant mass
XLabel=$m_{\mu^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+e^-}$ [fb/GeV]
XMin=0
Rebin=2
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_m
Title=$\mu^+e^-$ invariant mass
XLabel=$m_{\mu^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+e^-}$ [fb/GeV]
XMin=0
Rebin=2
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_m_log
Title=$\mu^+e^-$ invariant mass
XLabel=$m_{\mu^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+e^-}$ [fb/GeV]
XMin=0
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_m
Title=$\mu^-e^-$ invariant mass
XLabel=$m_{\mu^-e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^-e^-}$ [fb/GeV]
XMin=0
Rebin=2
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_electron_m_log
Title=$\mu^-e^-$ invariant mass
XLabel=$m_{\mu^-e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^-e^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_m_log
Title=$e^+e^-$ invariant mass
XLabel=$m_{e^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{e^+e^-}$ [fb/GeV]
XMin=0
XMax=2500
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_m
Title=$e^+e^-$ invariant mass
XLabel=$m_{e^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{e^+e^-}$ [fb/GeV]
XMin=0
Rebin=2
XMax=2500
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_m
Title=$\mu^+\mu^-$ invariant mass
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+\mu^-}$ [fb/GeV]
XMin=0
Rebin=2
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_m_log
Title=$\mu^+\mu^-$ invariant mass
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+\mu^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_pT
Title=$\mu^+\mu^-$ transverse momentum
XLabel=$p_{T,\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+\mu^-}$ [fb/GeV]
Rebin=2
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_pT_log
Title=$\mu^+\mu^-$ transverse momentum
XLabel=$p_{T,\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+\mu^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_antimuon_pT
Title=$\mu^+\mu^-$ transverse momentum
XLabel=$p_{T,\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+\mu^-}$ [fb/GeV]
Rebin=2
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_pT
Title=$\mu^-e^+$ transverse momentum
XLabel=$p_{T,\mu^-e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^-e^+}$ [fb/GeV]
Rebin=2
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_positron_pT_log
Title=$\mu^-e^+$ transverse momentum
XLabel=$p_{T,\mu^-e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^-e^+}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_pT
Title=$e^+e^-$ transverse momentum
XLabel=$p_{T,e^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^+e^-}$ [fb/GeV]
Rebin=2
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_pT_log
Title=$e^+e^-$ transverse momentum
XLabel=$p_{T,e^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^+e^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_m
Title=$\mu^+e^+$ invariant mass
XLabel=$m_{\mu^+e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+e^+}$ [fb/GeV]
XMin=0
Rebin=2
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_m_log
Title=$\mu^+e^+$ invariant mass
XLabel=$m_{\mu^+e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\mu^+e^+}$ [fb/GeV]
XMin=0
XMax=3000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_halved_pT
Title=$\mu^+e^-$ transverse momentum
XLabel=$p_{T,\mu^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+e^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_positron_halved_pT
Title=$\mu^+e^+$ transverse momentum
XLabel=$p_{T,\mu^+e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+e^+}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_halved_pT
Title=$e^+e^-$ transverse momentum
XLabel=$p_{T,e^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^+e^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/jet1_pT_full_halved
Title=leading-jet transverse momentum
XLabel=$p_{T,j_1}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,j_1}$ [fb/GeV]
XMin=25
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/jet1_pT_log
Title=leading-jet transverse momentum
XLabel=$p_{\text{T},j_1}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{\text{T},j_1}$ [fb/GeV]
XMin=25
XMax=2000
LegendXPos=0.1
LegendYPos=0.7
LogX=1
RatioPlotYMin=0.5001
RatioPlotYMax=1.4999
RatioPlotYMajorTickMarks=20
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/jet2_pT_log
Title=second-jet transverse momentum
XLabel=$p_{T,j_2}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,j_2}$ [fb/GeV]
XMin=25
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_pT
Title=$\mu^+e^-$ transverse momentum
XLabel=$p_{T,\mu^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+e^-}$ [fb/GeV]
XMin=0
Rebin=2
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_electron_pT_log
Title=$\mu^+e^-$ transverse momentum
XLabel=$p_{T,\mu^+e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+e^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_halved_pT
Title=$e^-$ transverse momentum
XLabel=$p_{T,e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_halved_pT
Title=$e^-$ transverse momentum
XLabel=$p_{T,e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/positron_halved_pT
Title=$e^+$ transverse momentum
XLabel=$p_{T,e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^+}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_halved_pT
Title=$\mu^+$ transverse momentum
XLabel=$p_{T,\mu^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_halved_pT
Title=$\mu^-$ transverse momentum
XLabel=$p_{T,\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^-}$ [fb/GeV]
XMin=0
XMax=2000
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_pT
Title=$\mu^-$ transverse momentum
XLabel=$p_{T,\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^-}$ [fb/GeV]
Rebin=2
XMin=0
XMax=1200
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muon_pT_log
Title=$\mu^-$ transverse momentum
XLabel=$p_{T,\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^-}$ [fb/GeV]
XMin=0
XMax=1200
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_pT_log
Title=$\mu^+$ transverse momentum
XLabel=$p_{T,\mu^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+}$ [fb/GeV]
XMin=0
XMax=1200
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuon_pT
Title=$\mu^+$ transverse momentum
XLabel=$p_{T,\mu^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,\mu^+}$ [fb/GeV]
Rebin=2
XMin=0
XMax=1200
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/positron_pT_log
Title=$e^+$ transverse momentum
XLabel=$p_{T,e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^+}$ [fb/GeV]
XMin=0
XMax=1200
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/positron_pT
Title=$e^+$ transverse momentum
XLabel=$p_{T,e^+}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^+}$ [fb/GeV]
Rebin=2
XMin=0
XMax=1200
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_pT
Title=$e^-$ transverse momentum
XLabel=$p_{T,e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^-}$ [fb/GeV]
Rebin=2
XMin=0
XMax=1200
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_pT_log
Title=$e^-$ transverse momentum
XLabel=$p_{T,e^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_{T,e^-}$ [fb/GeV]
XMin=0
XMax=1200
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_positron_dR
Title=$\Delta R_{e^+e^-}$
XLabel=$\Delta R_{e^+e^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{e^+e^-}$ [fb]
LogX=0
XMax=5
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuonjet1_dR
Title=$\Delta R_{j_1\mu^+}$
XLabel=$\Delta R_{j_1\mu^+}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{j_1\mu^+}$ [fb]
LogX=0
XMax=6
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muonjet1_dR
Title=$\Delta R_{j_1\mu^-}$
XLabel=$\Delta R_{j_1\mu^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{j_1\mu^-}$ [fb]
LogX=0
XMax=6
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/muonphoton_dR
Title=$\Delta R_{\mu^-\gamma}$
XLabel=$\Delta R_{\mu^-\gamma}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{\mu^-\gamma}$ [fb]
LogX=0
XMax=6
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electronjet1_dR
Title=$\Delta R_{j_1e^-}$
XLabel=$\Delta R_{j_1e^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{j_1e^-}$ [fb]
LogX=0
XMax=6
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/positronjet1_dR
Title=$\Delta R_{j_1e^+}$
XLabel=$\Delta R_{j_1e^+}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{j_1e^+}$ [fb]
LogX=0
XMax=6
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/antimuonphoton_dR
Title=$\Delta R_{\mu^+\gamma}$
XLabel=$\Delta R_{\mu^+\gamma}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{\mu^+\gamma}$ [fb]
LogX=0
XMax=8
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electronphoton_dR
Title=$\Delta R_{e^-\gamma}$
XLabel=$\Delta R_{e^-\gamma}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{e^-\gamma}$ [fb]
LogX=0
XMax=8
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/positronphoton_dR
Title=$\Delta R_{e^+\gamma}$
XLabel=$\Delta R_{e^+\gamma}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{e^+\gamma}$ [fb]
LogX=0
XMax=8
LegendXPos=0.65
LegendYPos=0.9
Scale=1e3
# END PLOT

# BEGIN PLOT .*
RightMargin=0.5
RatioPlotYLabel=Ratio to NLO EW
#RatioPlot1YMin=0.8
#RatioPlot1YMax=1.8
#RatioPlot2YMin=0.8
#RatioPlot2YMax=1.8
#RatioPlot1YLabel=$\sigma$/$\sigma_{\mathrm{NLO EW}\;\alpha(m_Z)}$
#RatioPlot2YLabel=$\sigma$/$\sigma_{\mathrm{NLO EW}\;G_\mu}$
Scale=1e3
# END PLOT


number_of_runs: 200 #50
premerge_multiplicity: 5
max_number_of_mpi_nodes: 4
walltime_hours:
  run: 23
  initialization: 0
  integration: 0
mem_mb:
  run: 4096
  initialization: 4096
  integration: 4096

rivet:
  custom_files: ["SS_ZZ_2e2mu_J30pT.plot"]
  custom_source_files: ["SS_ZZ_2e2mu_J30pT.cc"]
  analyses: ["SS_ZZ_2e2mu_J30pT"]
  args:
    #- "--mc-errs"
    #- "-m"
    #- "/SS_ZZ_2e2mu_J30pT/jet1_pT_log"
    #- "-m"
    #- "/SS_ZZ_2e2mu_J30pT/4l_j_dphi"
    - "-m"
    - "/SS_ZZ_2e2mu_J30pT/electron_y"
    - "-m"
    - "/SS_ZZ_2e2mu_J30pT/4l_m_log"
    - "-m"
    - "/SS_ZZ_2e2mu_J30pT/ZeeZmm_dR" 
    - "-m"
    - "/SS_ZZ_2e2mu_J30pT/Zee_pT$"

sherpas:
  default: &default_sherpa
    base: 3.0
    prefix: /home/bothmann/opt/sherpa-11-ew-sudakov-logs_production+Z-clustering+frozen-for-diboson-project
    plugins:
      libSherpaZZETScaleSetter.so:
        source_files:
        - masterZZ_Transverse_Energy_Scale_Setter.C
  ewvirt:
    <<: *default_sherpa
    prefix: /home/bothmann/opt/sherpa-tmp-cherrypick-ewvirt-into-master_production+frozen-for-diboson-project

integrations:
  default: &default_integration
    sherpa: default
    sherpa_configs: [Sherpa.yaml]
    args: []
  lo:
    <<: *default_integration
  lo_gmu:
    <<: *default_integration
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
  ewsuda_lo:
    <<: *default_integration
    sherpa_configs: [Sherpa.yaml, Sherpa_EWSud.yaml]
  ewsuda_lo_gmu:
    <<: *default_integration
    sherpa_configs: [Sherpa.yaml, Sherpa_EWSud.yaml]
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
  ewsuda_lo_zzj:
    <<: *default_integration
    sherpa_configs: [Sherpa_1j.yaml, Sherpa_EWSud_YFS_1j.yaml]
  ewsuda_lo_gmu_zzj:
    <<: *default_integration
    sherpa_configs: [Sherpa_1j.yaml, Sherpa_EWSud_YFS_1j.yaml]
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]

runs:
  lo: &lo_run
    integration: lo
    sherpa: default
    sherpa_configs: [Sherpa.yaml]
    events_per_run: 1M
    label: "LO $\\alpha(m_Z)$"
  lo_gmu:
    <<: *lo_run
    integration: lo_gmu
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
    label: "LO"  # $G_\\mu$"
  lo_gmu_yfs:
    <<: *lo_run
    integration: lo_gmu
    sherpa_configs: [Sherpa.yaml, Sherpa_YFS.yaml]
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
    label: "LO+YFS"  # $G_\\mu$"
  ewvirt:
    <<: *lo_run
    sherpa: ewvirt
    integration: lo
    sherpa_configs: [Sherpa.yaml, Sherpa_EWVirt.yaml]
    events_per_run: 100k
    label: "LO$+$EW$_\\mathrm{Virt}$ $\\alpha(m_Z)$"
  ewvirt_gmu:
    <<: *lo_run
    sherpa: ewvirt
    integration: lo_gmu
    sherpa_configs: [Sherpa.yaml, Sherpa_EWVirt.yaml]
    events_per_run: 100k
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
    label: "LO$+$EW$_\\mathrm{Virt}$"  # $G_\\mu$"
  ewvirt_yfs:
    <<: *lo_run
    sherpa: ewvirt
    integration: lo
    sherpa_configs: [Sherpa.yaml, Sherpa_EWVirt_YFS.yaml]
    events_per_run: 500k
    label: "LO$+$EW$_\\mathrm{Virt}$$+$YFS $\\alpha(m_Z)$"
  ewvirt_gmu_yfs:
    <<: *lo_run
    sherpa: ewvirt
    integration: lo_gmu
    sherpa_configs: [Sherpa.yaml, Sherpa_EWVirt_YFS.yaml]
    events_per_run: 500k
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
    label: "LO$+$EW$_\\mathrm{Virt}$$+$YFS"  # $G_\\mu$"
  ewsud:
    <<: *lo_run
    sherpa:  default
    integration: ewsuda_lo
    sherpa_configs: [Sherpa.yaml, Sherpa_EWSud.yaml]
    events_per_run: 1M
    label: "LO$+$EW$_\\mathrm{Sud}$ $\\alpha(m_Z)$"
    combinations: [EWSud]
    variation: EWSud
  ewsud_gmu:
    <<: *lo_run
    sherpa:  default
    integration: ewsuda_lo_gmu
    sherpa_configs: [Sherpa.yaml, Sherpa_EWSud.yaml]
    events_per_run: 1M
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
    label: "LO$+$EW$_\\mathrm{Sud}$"  # $G_\\mu$"
    combinations: [EWSud]
    variation: EWSud
  ewsud_yfs:
    <<: *lo_run
    sherpa:  default
    integration: ewsuda_lo
    sherpa_configs: [Sherpa.yaml, Sherpa_EWSud_YFS.yaml]
    events_per_run: 1M
    label: "LO$+$EW$_\\mathrm{Sud}$$+$YFS $\\alpha(m_Z)$"
    combinations: [EWSud]
    variation: EWSud
  ewsud_gmu_yfs: &ewsud_gmu_yfs_run
    <<: *lo_run
    sherpa:  default
    integration: ewsuda_lo_gmu
    sherpa_configs: [Sherpa.yaml, Sherpa_EWSud_YFS.yaml]
    events_per_run: 1M
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
    label: "LO$+$EW$_\\mathrm{Sud}$$+$YFS"  # $G_\\mu$"
    combinations: [EWSud]
    variation: EWSud
  ewsud_gmu_yfs_added_mass_terms:
    <<: *ewsud_gmu_yfs_run
  ewsud_gmu_yfs_ipi:
    <<: *ewsud_gmu_yfs_run
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1", "EWSUDAKOV_INCLUDE_I_PI=true"]
    label: "LO$+$EW$_\\mathrm{Sud}^\\mathrm{i\\pi}$$+$YFS"  # $G_\\mu$"
    combinations: [EWSud]
    variation: EWSud
  ewsud_yfs_zzj:
    <<: *lo_run
    sherpa:  default
    integration: ewsuda_lo
    sherpa_configs: [Sherpa_1j.yaml, Sherpa_EWSud_YFS_1j.yaml]
    events_per_run: 1M
    label: "LO$+$EW$_\\mathrm{Sud}$$+$YFS $\\alpha(m_Z)$"
    combinations: [EWSud]
    variation: EWSud
  ewsud_gmu_yfs_zzj: &ewsud_gmu_yfs_zzj_run
    <<: *lo_run
    sherpa:  default
    integration: ewsuda_lo_gmu_zzj
    sherpa_configs: [Sherpa_1j.yaml, Sherpa_EWSud_YFS_1j.yaml]
    events_per_run: 1M
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
    label: "LO$+$EW$_\\mathrm{Sud}$$+$YFS"  # $\\alpha(m_Z)$"
    combinations: [EWSud]
    variation: EWSud
  ewsud_gmu_zzj: &ewsud_gmu_zzj_run
    <<: *lo_run
    sherpa:  default
    integration: ewsuda_lo_gmu_zzj
    sherpa_configs: [Sherpa_1j.yaml, Sherpa_EWSud_1j.yaml]
    events_per_run: 1M
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1"]
    label: "LO$+$EW$_\\mathrm{Sud}$"  # $\\alpha(m_Z)$"
    combinations: [EWSud]
    variation: EWSud
  ewsud_gmu_yfs_zzj_added_mass_terms:
    <<: *ewsud_gmu_yfs_zzj_run
  ewsud_gmu_yfs_zzj_ipi:
    <<: *ewsud_gmu_yfs_zzj_run
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1", "EWSUDAKOV_INCLUDE_I_PI=true"]
    label: "LO$+$EW$_\\mathrm{Sud}^\\mathrm{i\\pi}$$+$YFS"  # $G_\\mu$"
    combinations: [EWSud]
    variation: EWSud
  ewsud_gmu_zzj_ipi:
    <<: *ewsud_gmu_zzj_run
    args: [EW_SCHEME=Gmu, "OL_EW_SCHEME:=1", "EWSUDAKOV_INCLUDE_I_PI=true"]
    label: "LO$+$EW$_\\mathrm{Sud}^\\mathrm{i\\pi}$"  # $G_\\mu$"
    combinations: [EWSud]
    variation: EWSud

global_combinations:
  kfac_nlo:
    method: uncorrelated_ratio
    input: [nlo, lo.nominal]
    label: "KFAC NLO"
  kfac_ewvirt:
    method: uncorrelated_ratio
    input: [ewvirt_yfs.nominal, lo.nominal]
    label: "KFAC EWVirt"
  kfac_ewsud:
    method: correlated_ratio
    input: [ewsud_yfs.EWSud, ewsud_yfs.nominal]
    label: "KFAC EWSud"
  kfac_diff_ewvirt:
    method: uncorrelated_difference
    input: [kfac_ewvirt, kfac_nlo]
    label: "$K_\\mathrm{NLO\\,EW} - K_{\\mathrm{LO}+\\mathrm{EWVirt}}$"
  kfac_diff_ewsud:
    method: uncorrelated_difference
    input: [kfac_ewsud, kfac_nlo]
    label: "$K_{\\mathrm{NLO\\,EW}} - K_{\\mathrm{LO}+\\mathrm{EWSud}}$"
  nlo_scheme_uncertainty:
    method: envelope
    input: [nlo, nlo_gmu]
    label: "NLO EW scheme uncert."
  nlo_scheme_uncertainty_1j:
    method: envelope
    input: [nlo_1j, nlo_gmu_1j]
    label: "NLO EW scheme uncert."
  nlo_nnl_matched:
    method: uncorrelated_sum
    input:
    - ewsud_exp_minus_order_alpha
    - nlo
    label: "NLO EW$+$NLL EW$_\\mathrm{Sud}^\\mathrm{exp}$"
  nlo_nnl_matched_gmu:
    method: uncorrelated_sum
    input:
    - ewsud_exp_minus_order_alpha_gmu
    - nlo_gmu 
    label: "NLO EW$+$NLL EW$_\\mathrm{Sud}^\\mathrm{exp}$"
  nlo_nnl_matched_bvi:
    method: uncorrelated_sum
    input:
    - ewsud_exp_minus_order_alpha_gmu
    - nlo_gmu_bvi
  nlo_nnl_matched_rs:
    method: calculate_and_apply_kfactor
    input:
    #- ewsud_exp_gmu_1j  not a good sample yet, use 0j as substitute
    - ewsud_gmu_yfs.EWSudKFacExp
    - ewsud_gmu_yfs.nominal
    - nlo_gmu_rs
  nlo_nnl_matched_bvikprs:
    method: uncorrelated_sum
    input:
    - nlo_nnl_matched_bvi
    - nlo_gmu_kp
    - nlo_nnl_matched_rs
  nlo_nnl_matched_gmu_1j:
    method: uncorrelated_sum
    input:
    - ewsud_exp_minus_order_alpha_gmu_1j
    - nlo_gmu_1j
    label: "NLO EW$+$NLL EW$_\\mathrm{Sud}^\\mathrm{exp}$"
  ewsud_exp_minus_order_alpha:
    method: correlated_difference
    input:
    - ewsud_yfs.EWSudKFacExp
    - ewsud_yfs.EWSudKFac
  ewsud_exp_minus_order_alpha_gmu:
    method: correlated_difference
    input:
    - ewsud_gmu_yfs.EWSudKFacExp
    - ewsud_gmu_yfs.EWSudKFac
  ewsud_exp_minus_order_alpha_gmu_1j:
    method: correlated_difference
    input:
    - ewsud_exp_gmu_1j_sub_cut
    - ewsud_gmu_1j_sub_cut

external_analysis_output:
  nlo:
    file: EW_0J_mZ.yoda
    label: "NLO EW $\\alpha(m_Z)$"
  nlo_gmu:
    file: EW_0J_Gmu.yoda
    label: "NLO EW"  # $G_\\mu$"
  nlo_gmu_bvi:
    file: BVI_Gmu.yoda
  nlo_gmu_kp:
    file: KP_Gmu.yoda
  nlo_gmu_rs:
    file: RS_Gmu.yoda
  nlo_jj:
    file: EW_0J_mZ_jj.yoda
    label: "NLO EW $\\alpha(m_Z)$ jj"
  nlo_jj_gmu:
    file: EW_0J_Gmu_jj.yoda
    label: "NLO EW jj"  # $G_\\mu$ jj"
  nlo_1j:
    file: 1J/Analysis_EWNLO/EW_1J_mZ_full.yoda
    label: "NLO EW $\\alpha(m_Z)$"
  nlo_gmu_1j:
    file: 1J/Analysis_EWNLO/EW_1J_Gmu_full.yoda
    label: "NLO EW"  # $G_\\mu$"
  nlo_jj_1j:
    file: 1J/Analysis_EWNLO/EW_1J_mZ_full_jj.yoda
    label: "NLO EW $\\alpha(m_Z)$ jj"
  nlo_jj_gmu_1j:
    file: 1J/Analysis_EWNLO/EW_1J_Gmu_full_jj.yoda
    label: "NLO EW (no interf.)"  # $G_\\mu$ (no interf.)"
  lo_gmu_slv:
    file: LO_0J/B_jj.yoda
    label: "LO"  # $G_\\mu$"
  lo_gmu_1j_slv:
    file: LO_1J/B_jj_1j.yoda
    label: "LO"  # $G_\\mu$"
  lo_gmu_1j:
    file: 1J/Analysis/Analysis.Nominal_gmu-noyfs.yoda
    label: "LO"  # $G_\\mu$"
  ewvirt_gmu_yfs_1j:
    file: 1J/Analysis_EWVirt/gmu-yfs.Nominal.yoda
    label: "LO$+$EW$_\\mathrm{Virt}$$+$YFS"  # $G_\\mu$"
  ewsud_gmu_1j:
    file: 1J/Analysis/Analysis.EWSudakov_gmu-yfs.yoda
    label: "LO$+$EW$_\\mathrm{Sud}$$+$YFS"  # $G_\\mu$"
  ewsud_exp_gmu_1j:
    file: 1J/Analysis/Analysis.EWSudakovExp_gmu-yfs.yoda
    label: "LO$+$EW$_\\mathrm{Sud}^\\mathrm{exp}$$+$YFS"  # $G_\\mu$"
  ewsud_gmu_1j_sub:
    file: 1J/Analysis/zzjets-ewsud-gmu-yfs-include-subleading-logs.Nominal.EWSud_KFactor.yoda
    label: "LO$+$EW$_\\mathrm{Sud,sublead}$$+$YFS"  # $G_\\mu$"
  ewsud_exp_gmu_1j_sub:
    file: 1J/Analysis/zzjets-ewsud-gmu-yfs-include-subleading-logs.Nominal.EWSud_KFactorExp.yoda
    label: "LO$+$EW$_\\mathrm{Sud,sublead}^\\mathrm{exp}$$+$YFS"  # $G_\\mu$"
  ewsud_gmu_1j_sub_cut:
    file: 1J/Analysis/zzjets-ewsud-gmu-yfs-include-subleading-logs-kfactorcut.Nominal.EWSud_KFactor.yoda
    label: "LO$+$EW$_\\mathrm{Sud}$$+$YFS"  # $G_\\mu$"
  ewsud_exp_gmu_1j_sub_cut:
    file: 1J/Analysis/zzjets-ewsud-gmu-yfs-include-subleading-logs-kfactorcut.Nominal.EWSud_KFactorExp.yoda
    label: "LO$+$EW$_\\mathrm{Sud}^\\mathrm{exp}$$+$YFS"  # $G_\\mu$"

colors:
  vibrant_orange:  [238,119, 51]
  vibrant_blue:    [  0,119,187]
  vibrant_cyan:    [ 51,187,238]
  vibrant_magenta: [238, 51,119]
  vibrant_red:     [204, 51, 17]
  vibrant_teal:    [  0,153,136]
  vibrant_grey:    [187,187,187]
  bright_blue:     [ 68,119,170]
  bright_red:      [238,102,119]
  bright_green:    [ 34,136, 51]
  bright_yellow:   [204,187, 68]
  bright_cyan:     [102,204,238]
  bright_purple:   [170, 51,119]
  bright_grey:     [187,187,187]
  marek_orange:    orange!90!black

comparisons:
  default:
    input: [nlo_scheme_uncertainty, nlo_gmu, nlo, lo_gmu_slv, ewvirt_gmu_yfs.nominal, ewsud_gmu_yfs.EWSudKFac, ewsud_gmu_yfs.EWSudKFacExp, nlo_nnl_matched_gmu]
    rivetmkhtml_plot_args:
      Title: $pp\to e^+e^-\mu^+\mu^-$, $\sqrt{s}=13\,\mathrm{TeV}$
      RatioPlotReference: nlo_gmu
      LegendOnly: nlo_gmu lo_gmu_slv ewvirt_gmu_yfs ewsud_gmu_yfsEWSudKFac ewsud_gmu_yfsEWSudKFacExp nlo_nnl_matched_gmu
      LegendEntrySeparation: 0.2
      RatioPlotDrawReferenceFirst: 1
    error_bands: [1, None]
    error_bars: [0, None]
    colors: [black, black, bright_grey, bright_blue, bright_red, bright_green, bright_green, marek_orange]
    styles: [None, solid, solid, solid, dashed, dotted, dashed, solid]
    error_band_styles: [vlines, None, None, None, None, None, None, None]
    hatch_colors: [bright_grey]
    legend_order: [1, 0, 0, 4, 5, 6, 7, 8]
    line_widths: [1.3pt, 1.3pt, 1.3pt, None, 1.3pt, 1.3pt, 1.3pt, 1.3pt]
    line_dash: [None, None, None, None, None, None, "3pt 3pt 0.8pt 3pt 0.8pt 3pt", None]
  default_1j:
    input: [nlo_scheme_uncertainty_1j, nlo_gmu_1j, nlo_1j, lo_gmu_1j_slv, ewvirt_gmu_yfs_1j, ewsud_gmu_1j_sub_cut, ewsud_exp_gmu_1j_sub_cut, nlo_nnl_matched_gmu_1j]
    rivetmkhtml_plot_args:
      Title: $pp\to \mathrm{e}^+\mathrm{e}^-\mu^+\mu^- j$, $\sqrt{s}=13\,\mathrm{TeV}$
      RatioPlotReference: nlo_gmu_1j
      LegendOnly: nlo_gmu_1j lo_gmu_1j_slv ewvirt_gmu_yfs_1j ewsud_gmu_1j_sub_cut ewsud_exp_gmu_1j_sub_cut nlo_nnl_matched_gmu_1j
      LegendEntrySeparation: 0.2
      RatioPlotDrawReferenceFirst: 1
    error_bands: [1, None]
    error_bars: [0, 1]
    colors: [black, black, bright_grey, bright_blue, bright_red, bright_green, bright_green, marek_orange]
    styles: [none, solid, solid, solid, dashed, dotted, dashed, solid]
    error_band_styles: [vlines, none, none, none, none, none, none, none]
    hatch_colors: [bright_grey]
    legend_order: [1, 0, 0, 4, 5, 6, 7, 8]
    line_widths: [1.3pt, 1.3pt, 1.3pt, None, 1.3pt, 1.3pt, 1.3pt, 1.3pt]
    line_dash: [None, None, None, None, None, None, "3pt 3pt 0.8pt 3pt 0.8pt 3pt", None]
  default_1j_jj:
    input: [nlo_scheme_uncertainty_1j, nlo_gmu_1j, nlo_jj_gmu_1j, nlo_1j, lo_gmu_1j_slv, ewvirt_gmu_yfs_1j, ewsud_gmu_1j_sub_cut, ewsud_exp_gmu_1j_sub_cut, nlo_nnl_matched_gmu_1j]
    rivetmkhtml_plot_args:
      Title: $pp\to \mathrm{e}^+\mathrm{e}^-\mu^+\mu^- j$, $\sqrt{s}=13\,\mathrm{TeV}$
      RatioPlotReference: nlo_gmu_1j
      LegendOnly: nlo_gmu_1j nlo_jj_gmu_1j lo_gmu_1j_slv ewvirt_gmu_yfs_1j ewsud_gmu_1j_sub_cut ewsud_exp_gmu_1j_sub_cut nlo_nnl_matched_gmu_1j
      LegendEntrySeparation: 0.2
      RatioPlotDrawReferenceFirst: 1
    error_bands: [1, None]
    error_bars: [0, 1]
    colors: [black, black, black, bright_grey, bright_blue, bright_red, bright_green, bright_green, marek_orange]
    styles: [none, solid, dashed, solid,       solid, dashed, dotted, dashed, solid, solid]
    error_band_styles: [vlines, none, none, none, none, none, none, none, none]
    hatch_colors: [bright_grey]
    legend_order: [1, 0, 0, 4, 5, 6, 7, 8, 9]
    line_widths: [1.3pt, 1.3pt, 1.3pt, 1.3pt, None, 1.3pt, 1.3pt, 1.3pt, 1.3pt]
    line_dash: [None, None, None, None, None, None, "3pt 3pt 0.8pt 3pt 0.8pt 3pt", None, None]
  kfacs:
    input: [kfac_nlo, kfac_ewvirt, kfac_ewsud]
    rivetmkhtml_plot_args:
      RatioPlotReference: kfac_nlo
      RatioPlot: 0
      LogY: 0
  kfacs_diff:
    input: [kfac_diff_ewvirt, kfac_diff_ewsud]
    rivetmkhtml_plot_args:
      LegendXPos: 0.35
      LegendYPos: 0.65
      XLabel: "$K$ Factor"
      RatioPlotReference: kfac_diff_ewvirt
      RatioPlot: 0
      LogY: 0
  ewsud:
    input: [ewsud.any]
  ewsud_subleading:
    input:
    - lo_gmu_1j
    - ewvirt_gmu_yfs_1j
    - ewsud_gmu_1j
    - ewsud_exp_gmu_1j
    - ewsud_gmu_1j_sub
    - ewsud_exp_gmu_1j_sub
    - ewsud_gmu_1j_sub_cut
    - ewsud_exp_gmu_1j_sub_cut
    rivetmkhtml_plot_args:
      RatioPlotReference: ewsud_gmu_1j

combinations:
  EWSud:
    method: nominal_plus_correlated_variations
    input: [nominal, C, LSC, PR, SSC, Yuk, Z]

variations:
  EWSud:
    EWSudKFac:
      sherpa_tag: EWSud_KFactor
      label: "LO$+$EW$_\\mathrm{Sud}$$+$YFS"  # $G_\\mu$"
    EWSudKFacExp:
      sherpa_tag: EWSud_KFactorExp
      #label: "LO$+\\exp($EW$_\\mathrm{Sud}$$-$$1)$$+$YFS"  # $G_\\mu$"
      label: "LO$+$EW$_\\mathrm{Sud}^\\mathrm{exp}$$+$YFS"
    C:              EWSud_C
    LSC:            EWSud_LSC
    SSC:            EWSud_SSC
    Yuk:            EWSud_Yuk
    Z:              EWSud_Z
    PR:             EWSud_PR
# vim: sw=2 ts=2 et

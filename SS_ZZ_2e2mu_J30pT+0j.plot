# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/ZeeZmm_dR
LegendXPos=0.4
LegendYPos=0.6
YLabelSep=7.5
LeftMargin=1.6
Title=Z-boson distance
XLabel=$\Delta R_{2e,2\mu}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{2e,2\mu}$ [fb]
LogX=0
XMax=8
YMax=2e1
YMin=5e-4
#Rebin=5
RatioPlotYMin=0.5001
RatioPlotYMax=1.4999
RatioPlotYMajorTickMarks=20
Scale=1e3
# END PLOT

# BEGIN PLOT /SS_ZZ_2e2mu_J30pT/electron_y
Title=${\text{e}^-}$ rapidity
XLabel=$y_{\text{e}^-}$
YLabel=$\text{d}\sigma/\text{d}y_{\text{e}^-}$ [fb]
YLabelSep=7.5
LeftMargin=1.6
LogX=0
LogY=0
XMin=-2.5
XMax=2.5
LegendXPos=0.35
LegendYPos=0.6
YMin=0
YMax=3
RatioPlotYMin=0.8001
RatioPlotYMax=1.1999
RatioPlotYMajorTickMarks=10
Scale=1e3
# END PLOT

# BEGIN SPECIAL .*
Location=MainPlot
\rput[bl]{-90}(1.01,1.00){\textsc{Sherpa+OpenLoops/Recola}}
#\rput[tl](0.06,0.15){$pp\to \text{e}^+\text{e}^- \mu^+ \mu^-$, $\sqrt{s}=13\,\mathrm{TeV}$}
#\rput[tl](1.0006,1.0){\rotatebox{-90}{\textbf{Sherpa+Recola}}}
#\rput[tl](0.8,-0.55){\textbf{with YFS}}
#\rput[tl]{0}(0.05,-0.05){$\alpha(m_Z)$}
#\rput[tl]{0}(0.05,-0.55){$G_\mu$}
# END SPECIAL

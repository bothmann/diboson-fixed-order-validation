// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"

namespace Rivet {


  /// MC validation analysis for Z[ee]Z[mumu] + jet events
  //  inclusive setup: SS_ZZ_2e2mu_J30pT
  //  incl. 2jet selection: SS_ZZ_2e2mu_J30pT_2j
  
  class SS_ZZ_2e2mu_J30pT : public Analysis {

    class NLOHisto1D : public YODA::Histo1D {

      YODA::Histo1D* _tmphist;
      int _current_event_number;

      void _syncHists() {
        for (size_t i=0; i<_tmphist->bins().size(); ++i) {
          if (_tmphist->bin(i).area()) YODA::Histo1D::fillBin(i, _tmphist->bin(i).area());
        }
        if (_tmphist->overflow().sumW())  YODA::Histo1D::overflow()+=_tmphist->overflow();
        if (_tmphist->underflow().sumW()) YODA::Histo1D::underflow()+=_tmphist->underflow();
        _tmphist->reset();
      }

    public:

      NLOHisto1D(size_t nbins, double lower, double upper, const string& path) :
        YODA::Histo1D(nbins, lower, upper, path),
        _current_event_number(-1)
      {
        _tmphist = new Histo1D(nbins, lower, upper, path+"_tmp");
      }
      
      NLOHisto1D(const vector<double>& binedges, const string& path) :
        YODA::Histo1D(binedges, path),
        _current_event_number(-1)
      {
        _tmphist = new Histo1D(binedges, path+"_tmp");
      }

      NLOHisto1D(const Scatter2D& refscatter, const string& path) :
        YODA::Histo1D(refscatter, path),
        _current_event_number(-1)
      {
        _tmphist = new Histo1D(refscatter, path+"_tmp");
      }

      ~NLOHisto1D()
      {
        delete _tmphist;
      }
        
      void fill(double x, const Event& event, const bool& useweight=true)
      {
        if (_current_event_number==-1)
          _current_event_number = event.genEvent()->event_number();

        if (event.genEvent()->event_number()!=_current_event_number) {
          _syncHists();
          _current_event_number = event.genEvent()->event_number();
        }

        _tmphist->fill(x, useweight?event.weight():1.);
      }

      void fillBin(size_t i, const Event& event, const double& fac)
      {
        if (_current_event_number==-1)
          _current_event_number = event.genEvent()->event_number();

        if (event.genEvent()->event_number()!=_current_event_number) {
          _syncHists();
          _current_event_number = event.genEvent()->event_number();
        }

        _tmphist->fillBin(i, event.weight()*fac);
      }

      void finalize()
      {
        _syncHists();
      }
    };

    typedef shared_ptr<NLOHisto1D> NLOHisto1DPtr;


    NLOHisto1DPtr bookNLOHisto1D(const string& hname,
                                size_t nbins, double lower, double upper)
    {
      NLOHisto1DPtr hist( new NLOHisto1D(nbins, lower, upper, histoPath(hname)) );
      addAnalysisObject(hist);
      return hist;
    }

    NLOHisto1DPtr bookNLOHisto1D(const string& hname,
                                const vector<double>& binedges)
    {
      NLOHisto1DPtr hist( new NLOHisto1D(binedges, histoPath(hname)) );
      addAnalysisObject(hist);
      return hist;
    }

    NLOHisto1DPtr bookNLOHisto1D(const string& hname,
                                const Scatter2D& refscatter) {
      NLOHisto1DPtr hist( new NLOHisto1D(refscatter, histoPath(hname)) );
      addAnalysisObject(hist);
      return hist;
    }

    NLOHisto1DPtr bookNLOHisto1D(const string& hname)
    {
      const Scatter2D& refdata = refData(hname);
      return bookNLOHisto1D(hname, refdata);
    }

    NLOHisto1DPtr bookNLOHisto1D(unsigned int datasetId, unsigned int xAxisId,
                                unsigned int yAxisId)
    {
      const string axisCode = makeAxisCode(datasetId, xAxisId, yAxisId);
      return bookNLOHisto1D(axisCode);
    }

  public:

    /// Default constructor
    SS_ZZ_2e2mu_J30pT(string name="SS_ZZ_2e2mu_J30pT")
      : Analysis(name)
    {
      _nmin=0;
      _nmax=1000;
    }


    /// @name Analysis methods
    //@{

    /// Book histograms
    void init() {
      FinalState fs;
      
      addProjection(fs, "FS");
      
      IdentifiedFinalState lfs;
      lfs.acceptIdPair(11);
      lfs.acceptIdPair(13);
      addProjection(lfs, "lepton_fs");
      IdentifiedFinalState pfs;
      pfs.acceptId(22);
      addProjection(pfs, "photon_fs");

      vector<PdgId> quark_ids;
      //quark_ids += PID::DQUARK, PID::UQUARK, PID::SQUARK, PID::CQUARK, -PID::DQUARK,-PID::UQUARK,-PID::SQUARK,-PID::CQUARK;
      quark_ids += PID::DQUARK;
      quark_ids += PID::UQUARK;
      quark_ids += PID::SQUARK;
      quark_ids += PID::CQUARK;
      quark_ids += -PID::DQUARK;
      quark_ids += -PID::UQUARK;
      quark_ids += -PID::SQUARK;
      quark_ids += -PID::CQUARK;
      
      const IdentifiedFinalState quark(quark_ids);
      addProjection(quark, "quark");
      const IdentifiedFinalState gluon(PID::GLUON);
      addProjection(gluon, "gluon");
       
      // Calculate XS after cuts:
      _h_xs = bookScatter2D("XS");
      _h_events_xs = bookNLOHisto1D("XS_events", 1, 0.0, 1.0);
      _h_veto_xs = bookNLOHisto1D("XS_veto", 1, 0.0, 1.0);

      // Monitor vetoes:
      _h_monitor_veto = bookNLOHisto1D("monitor_veto", 2, 0., 2.);

      // ANTI_KT ALGORITHM
      // Jet observables
      _h_jetmulti = bookNLOHisto1D("jet_multiplicity", 10, 0.0, 10.);
      _h_jet1_pT = bookNLOHisto1D("jet1_pT", 20, 0.0, 100.);
      _h_jet1_pT_full = bookNLOHisto1D("jet1_pT_full", 650, 0.0, 13000.);
      _h_jet1_pT_full_halved = bookNLOHisto1D("jet1_pT_full_halved", 325, 0.0, 13000.);
      _h_jet1_pT_log = bookNLOHisto1D("jet1_pT_log", logspace(20,25.,2500.));
      _h_jets_pT_full = bookNLOHisto1D("jets_pT_full", 650, 0.0, 13000.);
      _h_jets_pT_full_halved = bookNLOHisto1D("jets_pT_full_halved", 325, 0.0, 13000.);
      _h_jets_pT_full_log = bookNLOHisto1D("jets_pT_full_log", logspace(20,25.,2500.));
      _h_jet2_pT = bookNLOHisto1D("jet2_pT", 20, 0.0, 100.);
      _h_jet2_pT_log = bookNLOHisto1D("jet2_pT_log", logspace(20,25.,2500.));
      _h_jet1_y = bookNLOHisto1D("jet1_y", 80, -10., 10.);
      _h_jet2_y = bookNLOHisto1D("jet2_y", 80, -10., 10.);

      _h_electronjet1_dR = bookNLOHisto1D("electronjet1_dR", 105, 0., 21.);
      _h_positronjet1_dR = bookNLOHisto1D("positronjet1_dR", 105, 0., 21.);      
      _h_muonjet1_dR = bookNLOHisto1D("muonjet1_dR", 105, 0., 21.);
      _h_antimuonjet1_dR = bookNLOHisto1D("antimuonjet1_dR", 105, 0., 21.);
      _h_electronphoton_dR = bookNLOHisto1D("electronphoton_dR", 105, 0., 21.);
      _h_positronphoton_dR = bookNLOHisto1D("positronphoton_dR", 105, 0., 21.);
      _h_muonphoton_dR = bookNLOHisto1D("muonphoton_dR", 105, 0., 21.);
      _h_antimuonphoton_dR = bookNLOHisto1D("antimuonphoton_dR", 105, 0., 21.);
      
      // properties of the leptons
      _h_muon_pT = bookNLOHisto1D("muon_pT", 650, 0.0, 13000.);
      _h_muon_halved_pT = bookNLOHisto1D("muon_halved_pT", 325, 0.0, 13000.);
      _h_muon_pT_log = bookNLOHisto1D("muon_pT_log", logspace(20, 20.,2000.));
      _h_antimuon_pT = bookNLOHisto1D("antimuon_pT", 650, 0.0, 13000.);
      _h_antimuon_halved_pT = bookNLOHisto1D("antimuon_halved_pT", 325, 0.0, 13000.);
      _h_antimuon_pT_log = bookNLOHisto1D("antimuon_pT_log", logspace(20, 20.,2000.));
      _h_electron_pT = bookNLOHisto1D("electron_pT", 650, 0.0, 13000.);
      _h_electron_halved_pT = bookNLOHisto1D("electron_halved_pT", 325, 0.0, 13000.);
      _h_electron_pT_log = bookNLOHisto1D("electron_pT_log", logspace(20, 20.,2000.));
      _h_positron_pT = bookNLOHisto1D("positron_pT", 650, 0.0, 13000.);
      _h_positron_halved_pT = bookNLOHisto1D("positron_halved_pT", 325, 0.0, 13000.);
      _h_positron_pT_log = bookNLOHisto1D("positron_pT_log", logspace(20, 20.,2000.));
      
      _h_muon_y = bookNLOHisto1D("muon_y", 80, -10., 10.);
      _h_antimuon_y = bookNLOHisto1D("antimuon_y", 80, -10., 10.);
      _h_electron_y = bookNLOHisto1D("electron_y", 80, -10., 10.);      
      _h_positron_y = bookNLOHisto1D("positron_y", 80, -10., 10.);      
      // correlations between the charged leptons

      _h_muon_electron_pT = bookNLOHisto1D("muon_electron_pT", 650, 0.0, 13000.);
      _h_muon_electron_halved_pT = bookNLOHisto1D("muon_electron_halved_pT", 325, 0.0, 13000.);
      _h_muon_electron_pT_log = bookNLOHisto1D("muon_electron_pT_log", logspace(40,1.,10000.));
      
      _h_muon_positron_pT = bookNLOHisto1D("muon_positron_pT", 650, 0.0, 13000.);
      _h_muon_positron_halved_pT = bookNLOHisto1D("muon_positron_halved_pT", 325, 0.0, 13000.);
      _h_muon_positron_pT_log = bookNLOHisto1D("muon_positron_pT_log", logspace(40,1.,10000.));

      _h_electron_positron_pT = bookNLOHisto1D("electron_positron_pT", 650, 0.0, 13000.);
      _h_electron_positron_halved_pT = bookNLOHisto1D("electron_positron_halved_pT", 325, 0.0, 13000.);
      _h_electron_positron_pT_log = bookNLOHisto1D("electron_positron_pT_log", logspace(40,1.,10000.));

      _h_muon_antimuon_pT = bookNLOHisto1D("muon_antimuon_pT", 650, 0.0, 13000.);
      _h_muon_antimuon_halved_pT = bookNLOHisto1D("muon_antimuon_halved_pT", 325, 0.0, 13000.);
      _h_muon_antimuon_pT_log = bookNLOHisto1D("muon_antimuon_pT_log", logspace(40,1.,10000.));

      _h_muon_electron_m = bookNLOHisto1D("muon_electron_m", 650, 0.0, 13000.);
      _h_muon_electron_halved_m = bookNLOHisto1D("muon_electron_halved_m", 325, 0.0, 13000.);
      _h_muon_electron_m_log = bookNLOHisto1D("muon_electron_m_log", logspace(30,10.,10000.));

      _h_muon_positron_m = bookNLOHisto1D("muon_positron_m", 650, 0.0, 13000.);
      _h_muon_positron_halved_m = bookNLOHisto1D("muon_positron_halved_m", 325, 0.0, 13000.);
      _h_muon_positron_m_log = bookNLOHisto1D("muon_positron_m_log", logspace(30,10.,10000.));

      _h_antimuon_positron_m = bookNLOHisto1D("antimuon_positron_m", 650, 0.0, 13000.);
      _h_antimuon_positron_halved_m = bookNLOHisto1D("antimuon_positron_halved_m", 325, 0.0, 13000.);
      _h_antimuon_positron_m_log = bookNLOHisto1D("antimuon_positron_m_log", logspace(30,10.,10000.));

      _h_muon_positron_y = bookNLOHisto1D("muon_positron_y", 80, -10., 10.);
      _h_muon_positron_dR = bookNLOHisto1D("muon_positron_dR", 105, 0., 21.);
      _h_muon_positron_dphi = bookNLOHisto1D("muon_positron_dphi", 20, 0., 180.);
      _h_muon_positron_costheta = bookNLOHisto1D("muon_positron_costheta", 20, -1., 1.);

      _h_antimuon_electron_pT = bookNLOHisto1D("antimuon_electron_pT", 650, 0.0, 13000.);
      _h_antimuon_electron_halved_pT = bookNLOHisto1D("antimuon_electron_halved_pT", 325, 0.0, 13000.);
      _h_antimuon_electron_pT_log = bookNLOHisto1D("antimuon_electron_pT_log", logspace(40,1.,10000.));
      
      _h_antimuon_positron_pT = bookNLOHisto1D("antimuon_positron_pT", 650, 0.0, 13000.);
      _h_antimuon_positron_halved_pT = bookNLOHisto1D("antimuon_positron_halved_pT", 325, 0.0, 13000.);
      _h_antimuon_positron_pT_log = bookNLOHisto1D("antimuon_positron_pT_log", logspace(40,1.,10000.));
      
      _h_antimuon_electron_m = bookNLOHisto1D("antimuon_electron_m", 650, 0.0, 13000.);
      _h_antimuon_electron_halved_m = bookNLOHisto1D("antimuon_electron_halved_m", 325, 0.0, 13000.);
      _h_antimuon_electron_m_log = bookNLOHisto1D("antimuon_electron_m_log", logspace(30,10.,10000.));

      _h_electron_positron_m = bookNLOHisto1D("electron_positron_m", 650, 0.0, 13000.);
      _h_electron_positron_halved_m = bookNLOHisto1D("electron_positron_halved_m", 325, 0.0, 13000.);
      _h_electron_positron_m_log = bookNLOHisto1D("electron_positron_m_log", logspace(30,10.,10000.));

      _h_muon_positron_m = bookNLOHisto1D("muon_positron_m", 650, 0.0, 13000.);
      _h_muon_positron_halved_m = bookNLOHisto1D("muon_positron_halved_m", 325, 0.0, 13000.);
      _h_muon_positron_m_log = bookNLOHisto1D("muon_positron_m_log", logspace(30,10.,10000.));

      _h_muon_antimuon_m = bookNLOHisto1D("muon_antimuon_m", 650, 0.0, 13000.);
      _h_muon_antimuon_halved_m = bookNLOHisto1D("muon_antimuon_halved_m", 325, 0.0, 13000.);
      _h_muon_antimuon_m_log = bookNLOHisto1D("muon_antimuon_m_log", logspace(30,10.,10000.));

      _h_antimuon_electron_y = bookNLOHisto1D("antimuon_electron_y", 80, -10., 10.);
      _h_antimuon_electron_dR = bookNLOHisto1D("antimuon_electron_dR", 105, 0., 21.);
      _h_antimuon_electron_dphi = bookNLOHisto1D("antimuon_electron_dphi", 20, 0., 180.);
      _h_antimuon_electron_costheta = bookNLOHisto1D("antimuon_electron_costheta", 20, -1., 1.);

      _h_antimuon_positron_y = bookNLOHisto1D("antimuon_positron_y", 80, -10., 10.);
      _h_antimuon_positron_dR = bookNLOHisto1D("antimuon_positron_dR", 105, 0., 21.);
      _h_antimuon_positron_dphi = bookNLOHisto1D("antimuon_positron_dphi", 20, 0., 180.);
      _h_antimuon_positron_costheta = bookNLOHisto1D("antimuon_positron_costheta", 20, -1., 1.);

      _h_muon_electron_y = bookNLOHisto1D("muon_electron_y", 80, -10., 10.);
      _h_muon_electron_dR = bookNLOHisto1D("muon_electron_dR", 105, 0., 21.);
      _h_muon_electron_dphi = bookNLOHisto1D("muon_electron_dphi", 20, 0., 180.);
      _h_muon_electron_costheta = bookNLOHisto1D("muon_electron_costheta", 20, -1., 1.);

      _h_electron_positron_y = bookNLOHisto1D("electron_positron_y", 80, -10., 10.);
      _h_electron_positron_dR = bookNLOHisto1D("electron_positron_dR", 105, 0., 21.);
      _h_electron_positron_dphi = bookNLOHisto1D("electron_positron_dphi", 20, 0., 180.);
      _h_electron_positron_costheta = bookNLOHisto1D("electron_positron_costheta", 20, -1., 1.);

      _h_muon_antimuon_y = bookNLOHisto1D("muon_antimuon_y", 80, -10., 10.);
      _h_muon_antimuon_dR = bookNLOHisto1D("muon_antimuon_dR", 105, 0., 21.);
      _h_muon_antimuon_dphi = bookNLOHisto1D("muon_antimuon_dphi", 20, 0., 180.);
      _h_muon_antimuon_costheta = bookNLOHisto1D("muon_antimuon_costheta", 20, -1., 1.);
      
      // properties of the four leptons
      _h_4l_m = bookNLOHisto1D("4l_m", 650, 0.0, 13000.);
      _h_4l_halved_m = bookNLOHisto1D("4l_halved_m", 325, 0.0, 13000.);
      _h_4l_m_low = bookNLOHisto1D("4l_m_low", 100, 0., 500.);
      _h_4l_m_log = bookNLOHisto1D("4l_m_log", logspace(30,10.,10000.));
      _h_4l_j_dphi = bookNLOHisto1D("4l_j_dphi", 20, 0., 180.);

      // properties of the Zs
      _h_Zee_pT = bookNLOHisto1D("Zee_pT", 650, 0., 13000.);
      _h_Zee_pT_log = bookNLOHisto1D("Zee_pT_log",logspace(40,1.,10000.));
      _h_Zmm_pT = bookNLOHisto1D("Zmm_pT", 650, 0., 13000.);
      _h_Zmm_pT_log = bookNLOHisto1D("Zmm_pT_log",logspace(40,1.,10000.));
      _h_ZeeZmm_dphi = bookNLOHisto1D("ZeeZmm_dphi", 20, 0., PI);
      _h_ZeeZmm_dy = bookNLOHisto1D("ZeeZmm_dy", 50, 0., 10.);
      _h_ZeeZmm_dR = bookNLOHisto1D("ZeeZmm_dR", 105, 0., 21.);
      _h_Zee_m = bookNLOHisto1D("Zee_m", 250, 60., 560.);
      _h_Zmm_m = bookNLOHisto1D("Zmm_m", 250, 60., 560.);
    }

    /// Do the analysis
    void analyze(const Event & e) {
      //const double weight = e.weight();
      FourMomentum mass_4l, electron, positron, muon, antimuon;
      //std::cout << "Start analyze()\n";

      bool print_event = false;
      bool veto = false;
      bool recombination = false;

      ParticleVector cand_quarks = applyProjection<IdentifiedFinalState>(e, "quark").particlesByPt();
      ParticleVector cand_gluons = applyProjection<IdentifiedFinalState>(e, "gluon").particlesByPt();

      const FinalState& fs = applyProjection<FinalState>(e, "FS");
      ParticleVector leptons = applyProjection<IdentifiedFinalState>(e, "lepton_fs").particlesByPt();
      ParticleVector photons = applyProjection<IdentifiedFinalState>(e, "photon_fs").particlesByPt();


      double maxd(10000), dmin(maxd);
      double recom_cut(0.1);
      size_t ii(100000), jj(0);
             
      for (size_t i(0); i<leptons.size(); ++i) {
        for (size_t j(0); j<photons.size(); ++j) {
          double dij(deltaR(leptons[i].momentum(), photons[j].momentum(), RAPIDITY)/recom_cut);
          if (dij<dmin && dij<1) { dmin=dij; ii=i; jj=j; }
        }
      }
      if (cand_quarks.size()>0) {
        for (size_t i(0); i<cand_quarks.size(); ++i) {
          for (size_t j(0); j<photons.size(); ++j) {
            double dij(deltaR(cand_quarks[i].momentum(), photons[j].momentum(), RAPIDITY)/recom_cut);
            if (dij<dmin && dij<1) { dmin=dij; ii=i+100; jj=j; }
          }
        }
      }
      if (cand_gluons.size()>0) {
        for (size_t i(0); i<cand_gluons.size(); ++i) {
          for (size_t j(0); j<photons.size(); ++j) {
            double dij(deltaR(cand_gluons[i].momentum(), photons[j].momentum(), RAPIDITY)/recom_cut);
            if (dij<dmin && dij<1) { dmin=dij; ii=i+200; jj=j; }
          }
        }
      }

      if (ii<100000) recombination=true;

      //determined smallest dij; combine and update momenta (how to do this in rivet?)
      for (size_t i(0); i<leptons.size(); ++i) {
        if (ii<100 && i==ii) {
          FourMomentum newmom;
          newmom = leptons[ii].momentum() + photons[jj].momentum();
          if (leptons[i].pdgId()==11) electron = newmom;
          if (leptons[i].pdgId()==-11) positron = newmom;
          if (leptons[i].pdgId()==13) muon = newmom;
          if (leptons[i].pdgId()==-13) antimuon = newmom;
        }
        else {
          if (leptons[i].pdgId()==11) electron = leptons[i].momentum();
          if (leptons[i].pdgId()==-11) positron = leptons[i].momentum();
          if (leptons[i].pdgId()==13) muon = leptons[i].momentum();
          if (leptons[i].pdgId()==-13) antimuon = leptons[i].momentum();
        }
      }
      
      for (size_t i(0); i<cand_quarks.size(); ++i) {
        if (i==ii-100 && ii>=100 && ii<200) {
          FourMomentum newmom;
          newmom = photons[jj].momentum()+cand_quarks[i].momentum();
          cand_quarks[i].setMomentum(newmom);
        }
      }
      for (size_t i(0); i<cand_gluons.size(); ++i) {
        if (i==ii-200 && ii>=200) {
          FourMomentum newmom;
          newmom = photons[jj].momentum()+cand_gluons[i].momentum();
          cand_gluons[i].setMomentum(newmom);
        }
      }

      // * Lepton |eta| < 2.5 and pT > 20 GeV cuts
      if (electron.pT() < 20. || electron.absrapidity() > 2.5) {
        veto = true;
        //std::cout << "electron cuts not passed\n";
      }
      if (positron.pT() < 20. || positron.absrapidity() > 2.5) {
        veto = true;
        //std::cout << "positron cuts not passed\n";
      }
      if (muon.pT() < 20. || muon.absrapidity() > 2.5) {
        veto = true;
        //std::cout << "muon cuts not passed\n";
      }
      if (antimuon.pT() < 20. || antimuon.absrapidity() > 2.5) {
        veto = true;
        //std::cout << "antimuon cuts not passed\n";
      }
          
      // * Lepton Delta R > 0.1 and Mass > 10 GeV
      if (deltaR(electron,positron, RAPIDITY) < 0.1) {
        veto = true;
        //std::cout << "DR(e,p, RAPIDITY) = " << deltaR(electron,positron, RAPIDITY) << " < 0.1\n";
      }
      if (deltaR(electron,muon, RAPIDITY) < 0.1) {
        veto = true;
        //std::cout << "DR(e,mu, RAPIDITY) = " << deltaR(electron,muon, RAPIDITY) << " < 0.1\n";
      }
      if (deltaR(electron,antimuon, RAPIDITY) < 0.1) {
        veto = true;
        //std::cout << "DR(e,amu, RAPIDITY) = " << deltaR(electron,antimuon, RAPIDITY) << " < 0.1\n";
      }
      if (deltaR(positron,muon, RAPIDITY) < 0.1) {
        veto = true;
        //std::cout << "DR(e+,mu, RAPIDITY) = " << deltaR(positron,muon, RAPIDITY) << " < 0.1\n";
      }
      if (deltaR(positron,antimuon, RAPIDITY) < 0.1) {
        veto = true;
        //std::cout << "DR(e+,amu, RAPIDITY) = " << deltaR(positron,antimuon, RAPIDITY) << " < 0.1\n";
      }
      if (deltaR(muon,antimuon, RAPIDITY) < 0.1) {
        veto = true;
        //std::cout << "DR(mu,amu, RAPIDITY) = " << deltaR(muon,antimuon, RAPIDITY) << " < 0.1\n";
      }

      FourMomentum muon_electron = muon + electron;
      FourMomentum muon_antimuon = muon + antimuon;
      FourMomentum muon_positron = muon + positron;
      FourMomentum electron_positron = electron + positron;
      FourMomentum antimuon_electron = antimuon + electron;
      FourMomentum antimuon_positron = antimuon + positron;
      mass_4l = electron + positron + muon + antimuon;
      // if (muon_electron.mass() < 10.) {
      //         veto = true;
      //         //std::cout << "muon_electron mass = " << muon_electron.mass() << " > 10.\n";
      // }

      vector<PseudoJet> jetinput; //only qcd partons
      foreach (const Particle& p, cand_quarks) jetinput.push_back(p);
      foreach (const Particle& p, cand_gluons) jetinput.push_back(p);

      fastjet::ClusterSequence cs(jetinput, fastjet::JetDefinition(fastjet::antikt_algorithm, 0.4));
      vector<PseudoJet> pseudo_jets = sorted_by_pt(cs.inclusive_jets());

      Jets jets;
      double dr_jl(100000);
      foreach (const PseudoJet& pseudo_jet, pseudo_jets) {
              if (pseudo_jet.perp() > 30. && fabs(pseudo_jet.rapidity()) < 4.5) {
               jets.push_back(pseudo_jet);        
              }
      }
      foreach (const Jet& j, jets) {
        double dr_je = deltaR(j.momentum(),electron, RAPIDITY);
        double dr_jm = deltaR(j.momentum(),muon, RAPIDITY);
        double dr_jp = deltaR(j.momentum(),positron, RAPIDITY);
        double dr_jam = deltaR(j.momentum(),antimuon, RAPIDITY);
        if (dr_je < dr_jl) dr_jl = dr_je;
        if (dr_jm < dr_jl) dr_jl = dr_jm;
        if (dr_jp < dr_jl) dr_jl = dr_jp;
        if (dr_jam < dr_jl) dr_jl = dr_jam;
      }

      if (_nmin>jets.size() || jets.size()>_nmax) {
        std::cout << jets.size()-1 << "st jet pt>30, veto\n";
        veto = true;
      }

      // * request Jet - Lepton separation of DR=0.4
      if (dr_jl < 0.4) {
        //std::cout << "minimal jet/lepton separation cut veto, dr_min = " <<dr_jl<<std::endl;
        veto = true;
      }

      // VetoEvent if one check not passed
      //if (veto) vetoEvent;
      if (veto) {
        _h_monitor_veto->fill(0.5, e);
        vetoEvent;
      }
      else {
        _h_monitor_veto->fill(1.5, e);
      }
      //std::cout << "Passed all vetoes" << std::endl;

      // Fill histograms      
      if (!recombination && photons.size()>0) {
        _h_electronphoton_dR->fill(deltaR(electron,photons[0].momentum()), e);
        _h_positronphoton_dR->fill(deltaR(positron,photons[0].momentum()), e);
        _h_muonphoton_dR->fill(deltaR(muon,photons[0].momentum()), e);
        _h_antimuonphoton_dR->fill(deltaR(antimuon,photons[0].momentum()), e);
      }

      _h_jetmulti->fill(jets.size(), e);

      if (jets.size()) {
        _h_jet1_pT->fill(jets[0].pT(), e);
        _h_jet1_pT_full->fill(jets[0].pT(), e);
        _h_jet1_pT_full_halved->fill(jets[0].pt(), e);
        _h_jet1_pT_log->fill(jets[0].pT(), e);
        for (size_t i(0); i<jets.size(); i++) {
          _h_jets_pT_full->fill(jets[i].pT(), e);
          _h_jets_pT_full_halved->fill(jets[i].pt(), e);
          _h_jets_pT_full_log->fill(jets[i].pT(), e);
        }
        _h_jet1_y->fill(jets[0].rapidity(), e);
        if (jets.size()>1) {
          _h_jet2_pT->fill(jets[1].pT(), e);
          _h_jet2_pT_log->fill(jets[1].pT(), e);
          _h_jet2_y->fill(jets[1].rapidity(), e);
        }

        _h_electronjet1_dR->fill(deltaR(electron,jets[0].momentum()), e);
        _h_positronjet1_dR->fill(deltaR(positron,jets[0].momentum()), e);
        _h_muonjet1_dR->fill(deltaR(muon,jets[0].momentum()), e);
        _h_antimuonjet1_dR->fill(deltaR(antimuon,jets[0].momentum()), e);
        // ADDED
        FourMomentum ZZ(electron+positron+muon+antimuon);
        _h_4l_j_dphi->fill(deltaPhi(ZZ,jets[0].momentum())/M_PI*180.,e);
      }

      _h_electron_pT->fill(electron.pT(), e);
      _h_electron_halved_pT->fill(electron.pT(), e);
      _h_electron_pT_log->fill(electron.pT(), e);
      _h_electron_y->fill(electron.rapidity(), e);

      _h_positron_pT->fill(positron.pT(), e);
      _h_positron_halved_pT->fill(positron.pT(), e);
      _h_positron_pT_log->fill(positron.pT(), e);
      _h_positron_y->fill(positron.rapidity(), e);

      _h_muon_pT->fill(muon.pT(), e);
      _h_muon_halved_pT->fill(muon.pT(), e);
      _h_muon_pT_log->fill(muon.pT(), e);
      _h_muon_y->fill(muon.rapidity(), e);

      _h_antimuon_pT->fill(antimuon.pT(), e);
      _h_antimuon_halved_pT->fill(antimuon.pT(), e);
      _h_antimuon_pT_log->fill(antimuon.pT(), e);
      _h_antimuon_y->fill(antimuon.rapidity(), e);
        
      _h_4l_m->fill(mass_4l.mass(), e);
      _h_4l_halved_m->fill(mass_4l.mass(), e);
      _h_4l_m_low->fill(mass_4l.mass(), e);
      _h_4l_m_log->fill(mass_4l.mass(), e);
      
      _h_muon_electron_pT->fill(muon_electron.pT(), e);
      _h_muon_electron_halved_pT->fill(muon_electron.pT(), e);
      _h_muon_electron_pT_log->fill(muon_electron.pT(), e);
      _h_muon_electron_m->fill(muon_electron.mass(), e);
      _h_muon_electron_halved_m->fill(muon_electron.mass(), e);
      _h_muon_electron_m_log->fill(muon_electron.mass(), e);
      _h_muon_electron_y->fill(muon_electron.rapidity(), e);
 
      _h_muon_positron_pT->fill(muon_positron.pT(), e);
      _h_muon_positron_halved_pT->fill(muon_positron.pT(), e);
      _h_muon_positron_pT_log->fill(muon_positron.pT(), e);

      _h_muon_positron_m->fill(muon_positron.mass(), e);
      _h_muon_positron_halved_m->fill(muon_positron.mass(), e);
      _h_muon_positron_m_log->fill(muon_positron.mass(), e);
      _h_muon_positron_y->fill(muon_positron.rapidity(), e);
  
      _h_muon_antimuon_pT->fill(muon_antimuon.pT(), e);
      _h_muon_antimuon_halved_pT->fill(muon_antimuon.pT(), e);
      _h_muon_antimuon_pT_log->fill(muon_antimuon.pT(), e);
      _h_muon_antimuon_m->fill(muon_antimuon.mass(), e);
      _h_muon_antimuon_halved_m->fill(muon_antimuon.mass(), e);
      _h_muon_antimuon_m_log->fill(muon_antimuon.mass(), e);
      _h_muon_antimuon_y->fill(muon_antimuon.rapidity(), e);
      
      _h_electron_positron_pT->fill(electron_positron.pT(), e);
      _h_electron_positron_halved_pT->fill(electron_positron.pT(), e);
      _h_electron_positron_pT_log->fill(electron_positron.pT(), e);
      _h_electron_positron_m->fill(electron_positron.mass(), e);
      _h_electron_positron_halved_m->fill(electron_positron.mass(), e);
      _h_electron_positron_m_log->fill(electron_positron.mass(), e);
      _h_electron_positron_y->fill(electron_positron.rapidity(), e);

      _h_antimuon_positron_pT->fill(antimuon_positron.pT(), e);
      _h_antimuon_positron_halved_pT->fill(antimuon_positron.pT(), e);
      _h_antimuon_positron_pT_log->fill(antimuon_positron.pT(), e);
      _h_antimuon_positron_m->fill(antimuon_positron.mass(), e);
      _h_antimuon_positron_halved_m->fill(antimuon_positron.mass(), e);
      _h_antimuon_positron_m_log->fill(antimuon_positron.mass(), e);
      _h_antimuon_positron_y->fill(antimuon_positron.rapidity(), e);
      
      _h_antimuon_electron_pT->fill(antimuon_electron.pT(), e);
      _h_antimuon_electron_halved_pT->fill(antimuon_electron.pT(), e);
      _h_antimuon_electron_pT_log->fill(antimuon_electron.pT(), e);
      _h_antimuon_electron_m->fill(antimuon_electron.mass(), e);
      _h_antimuon_electron_halved_m->fill(antimuon_electron.mass(), e);
      _h_antimuon_electron_m_log->fill(antimuon_electron.mass(), e);
      _h_antimuon_electron_y->fill(antimuon_electron.rapidity(), e);

      _h_muon_electron_dR->fill(deltaR(muon,electron), e);
      _h_muon_positron_dR->fill(deltaR(muon,positron), e);
      _h_antimuon_electron_dR->fill(deltaR(antimuon,electron), e);
      _h_antimuon_positron_dR->fill(deltaR(antimuon,positron), e);
      _h_muon_antimuon_dR->fill(deltaR(muon,antimuon), e);
      _h_electron_positron_dR->fill(deltaR(electron,positron), e);

      double angle, costheta;
      // phi
      angle = deltaPhi(muon.phi(),electron.phi())/M_PI*180.;
      _h_muon_electron_dphi->fill(angle, e);
      
      angle = deltaPhi(muon.phi(),positron.phi())/M_PI*180.;
      _h_muon_positron_dphi->fill(angle, e);

      angle = deltaPhi(muon.phi(),antimuon.phi())/M_PI*180.;
      _h_muon_antimuon_dphi->fill(angle, e);
      
      angle = deltaPhi(antimuon.phi(),electron.phi())/M_PI*180.;
      _h_antimuon_electron_dphi->fill(angle, e);

      angle = deltaPhi(antimuon.phi(),positron.phi())/M_PI*180.;
      _h_antimuon_positron_dphi->fill(angle, e);

      angle = deltaPhi(electron.phi(),positron.phi())/M_PI*180.;
      _h_electron_positron_dphi->fill(angle, e);

 
      // costheta
      costheta = cos(muon.angle(electron));
      _h_muon_electron_costheta->fill(costheta, e);

      costheta = cos(muon.angle(positron));
      _h_muon_positron_costheta->fill(costheta, e);

      costheta = cos(muon.angle(antimuon));
      _h_muon_antimuon_costheta->fill(costheta, e);

      costheta = cos(antimuon.angle(electron));
      _h_antimuon_electron_costheta->fill(costheta, e);

      costheta = cos(antimuon.angle(positron));
      _h_antimuon_positron_costheta->fill(costheta, e);

      costheta = cos(electron.angle(positron));
      _h_electron_positron_costheta->fill(costheta, e);


      FourMomentum Zee(electron+positron),Zmm(muon+antimuon);
      _h_Zee_pT->fill(Zee.pT(), e);
      _h_Zee_pT_log->fill(Zee.pT(), e);
      _h_Zmm_pT->fill(Zmm.pT(), e);
      _h_Zmm_pT_log->fill(Zmm.pT(), e);
      _h_ZeeZmm_dphi->fill(deltaPhi(Zee,Zmm), e);
      _h_ZeeZmm_dy->fill(deltaRap(Zee,Zmm), e);
      _h_ZeeZmm_dR->fill(deltaR(Zee,Zmm), e);
      _h_Zee_m->fill(Zee.mass(), e);
      _h_Zmm_m->fill(Zmm.mass(), e);

      if (print_event) {
        std::cout << "SS_ZZ_2e2mu_J30pT: " << std::endl;
        foreach (const Particle& p, fs.particles()) {        
          std::cout << "Particle " << p.pid() << std::endl;
          std::cout << "Momentum " << p.momentum() << std::endl;
          if (p.pid() == 11) std::cout << "Electron_Momentum " << electron << std::endl;          
          if (p.pid() == -11) std::cout << "Positron_Momentum " << positron << std::endl;          
          if (p.pid() == 13) std::cout << "Muon_Momentum " << muon << std::endl;
          if (p.pid() == -13) std::cout << "Antimuon_Momentum " << antimuon << std::endl;
          std::cout << "Eta " << p.eta() << std::endl;
          std::cout << "Rapidity " << p.rapidity() << std::endl;
          std::cout << "angle " << p.phi() << std::endl;
          if (p.pid() != 22 && photons.size()>0) {
            std::cout << "deltaR(Particle, Photon) " << deltaR(p,photons[0],RAPIDITY) << std::endl;
            std::cout << "deltaR(Particle, Photon, pseudorapidity) " << deltaR(p,photons[0]) << std::endl;
          }
        }
        std::cout << "reconstructed jets:\n";
        foreach (const Jet& j, jets) {          
          std::cout << j.momentum() << ", ";
        }
        std::cout << "\n";
        std::cout << "out of the pseudo_jets:\n";
        foreach (const PseudoJet& pj, pseudo_jets) {
          std::cout << "("<<pj.E()<<", "<<pj.px()<<", "<<pj.py()<<", "<<pj.pz()<<"), ";
        }
        std::cout << "\n";
      }
      #ifdef HEPMC_HAS_CROSS_SECTION
      _var_xs = e.genEvent()->cross_section()->cross_section();
      _var_error = e.genEvent()->cross_section()->cross_section_error();
      #endif

      _h_events_xs->fill(0.5,e);
      if (veto) _h_veto_xs->fill(0.5,e);

    }


    /// Finalize
    void finalize() {

      #ifndef HEPMC_HAS_CROSS_SECTION
      _var_xs = crossSection();
      _var_error = 0.0;
      #endif
      _h_xs->addPoint(0, _var_xs, 0.5, _var_error);

      const double norm = crossSection()/sumOfWeights();
      scale(_h_events_xs, norm);
      scale(_h_veto_xs, norm);
      scale(_h_monitor_veto, norm);

      scale(_h_jetmulti, norm);
      scale(_h_jet1_pT, norm);
      scale(_h_jet1_pT_full, norm);
      scale(_h_jet1_pT_full_halved, norm);
      scale(_h_jet1_pT_log, norm);
      scale(_h_jets_pT_full, norm);
      scale(_h_jets_pT_full_halved, norm);
      scale(_h_jets_pT_full_log, norm);
      scale(_h_jet2_pT, norm);
      scale(_h_jet2_pT_log, norm);
      scale(_h_jet1_y, norm);
      scale(_h_jet2_y, norm);
      scale(_h_electronjet1_dR, norm);
      scale(_h_positronjet1_dR, norm);
      scale(_h_muonjet1_dR, norm);
      scale(_h_antimuonjet1_dR, norm);
      scale(_h_electronphoton_dR, norm);
      scale(_h_positronphoton_dR, norm);
      scale(_h_muonphoton_dR, norm);
      scale(_h_antimuonphoton_dR, norm);
      scale(_h_muon_pT, norm);
      scale(_h_muon_halved_pT, norm);
      scale(_h_muon_pT_log, norm);
      scale(_h_antimuon_pT, norm);
      scale(_h_antimuon_halved_pT, norm);
      scale(_h_antimuon_pT_log, norm);
      scale(_h_electron_pT, norm);
      scale(_h_electron_halved_pT, norm);
      scale(_h_electron_pT_log, norm);
      scale(_h_positron_pT, norm);
      scale(_h_positron_halved_pT, norm);
      scale(_h_positron_pT_log, norm);
      scale(_h_muon_y, norm);
      scale(_h_antimuon_y, norm);
      scale(_h_electron_y, norm);
      scale(_h_positron_y, norm);
      scale(_h_muon_electron_pT, norm);
      scale(_h_muon_electron_halved_pT, norm);
      scale(_h_muon_electron_pT_log, norm);
      scale(_h_muon_electron_m, norm);
      scale(_h_muon_electron_halved_m, norm);
      scale(_h_muon_electron_m_log, norm);
      scale(_h_muon_electron_y, norm);
      scale(_h_muon_electron_dR, norm);
      scale(_h_muon_electron_dphi, norm);
      scale(_h_muon_electron_costheta, norm);
      scale(_h_muon_positron_pT, norm);
      scale(_h_muon_positron_halved_pT, norm);
      scale(_h_muon_positron_pT_log, norm);
      scale(_h_muon_positron_m, norm);
      scale(_h_muon_positron_halved_m, norm);
      scale(_h_muon_positron_m_log, norm);
      scale(_h_muon_positron_y, norm);
      scale(_h_muon_positron_dR, norm);
      scale(_h_muon_positron_dphi, norm);
      scale(_h_muon_positron_costheta, norm);
      scale(_h_electron_positron_pT, norm);
      scale(_h_electron_positron_halved_pT, norm);
      scale(_h_electron_positron_pT_log, norm);
      scale(_h_electron_positron_m, norm);
      scale(_h_electron_positron_halved_m, norm);
      scale(_h_electron_positron_m_log, norm);
      scale(_h_electron_positron_y, norm);
      scale(_h_electron_positron_dR, norm);
      scale(_h_electron_positron_dphi, norm);
      scale(_h_electron_positron_costheta, norm);
      scale(_h_antimuon_positron_pT, norm);
      scale(_h_antimuon_positron_halved_pT, norm);
      scale(_h_antimuon_positron_pT_log, norm);
      scale(_h_antimuon_positron_m, norm);
      scale(_h_antimuon_positron_halved_m, norm);
      scale(_h_antimuon_positron_m_log, norm);
      scale(_h_antimuon_positron_y, norm);
      scale(_h_antimuon_positron_dR, norm);
      scale(_h_antimuon_positron_dphi, norm);
      scale(_h_antimuon_positron_costheta, norm);
      scale(_h_antimuon_electron_pT, norm);
      scale(_h_antimuon_electron_halved_pT, norm);
      scale(_h_antimuon_electron_pT_log, norm);
      scale(_h_antimuon_electron_m, norm);
      scale(_h_antimuon_electron_halved_m, norm);
      scale(_h_antimuon_electron_m_log, norm);
      scale(_h_antimuon_electron_y, norm);
      scale(_h_antimuon_electron_dR, norm);
      scale(_h_antimuon_electron_dphi, norm);
      scale(_h_antimuon_electron_costheta, norm);
      scale(_h_muon_antimuon_pT, norm);
      scale(_h_muon_antimuon_halved_pT, norm);
      scale(_h_muon_antimuon_pT_log, norm);
      scale(_h_muon_antimuon_m, norm);
      scale(_h_muon_antimuon_halved_m, norm);
      scale(_h_muon_antimuon_m_log, norm);
      scale(_h_muon_antimuon_y, norm);
      scale(_h_muon_antimuon_dR, norm);
      scale(_h_muon_antimuon_dphi, norm);
      scale(_h_muon_antimuon_costheta, norm);

      scale(_h_4l_m, norm);
      scale(_h_4l_halved_m, norm);
      scale(_h_4l_m_low, norm);
      scale(_h_4l_m_log, norm);
      scale(_h_Zee_pT, norm);
      scale(_h_Zee_pT_log, norm);
      scale(_h_Zmm_pT, norm);
      scale(_h_Zmm_pT_log, norm);
      scale(_h_ZeeZmm_dphi, norm);
      scale(_h_ZeeZmm_dy, norm);
      scale(_h_ZeeZmm_dR, norm);
      scale(_h_Zee_m, norm);
      scale(_h_Zmm_m, norm);
      scale(_h_4l_j_dphi, norm);
    }

    //@}


  protected:

    /// @name Histograms
    //@{
    NLOHisto1DPtr _h_veto_xs;
    NLOHisto1DPtr _h_events_xs;
    NLOHisto1DPtr _h_monitor_veto;
    Scatter2DPtr _h_xs;
    double _var_xs, _var_error;
    size_t _nmin,_nmax;

    NLOHisto1DPtr _h_jetmulti;
    NLOHisto1DPtr _h_jet1_pT;
    NLOHisto1DPtr _h_jet1_pT_full;
    NLOHisto1DPtr _h_jet1_pT_full_halved;
    NLOHisto1DPtr _h_jet1_pT_log;
    NLOHisto1DPtr _h_jets_pT_full;
    NLOHisto1DPtr _h_jets_pT_full_halved;
    NLOHisto1DPtr _h_jets_pT_full_log;
    NLOHisto1DPtr _h_jet2_pT;
    NLOHisto1DPtr _h_jet2_pT_log;
    NLOHisto1DPtr _h_jet1_y;
    NLOHisto1DPtr _h_jet2_y;
    NLOHisto1DPtr _h_electronjet1_dR;
    NLOHisto1DPtr _h_muonjet1_dR;
    NLOHisto1DPtr _h_positronjet1_dR;
    NLOHisto1DPtr _h_antimuonjet1_dR;
    NLOHisto1DPtr _h_electronphoton_dR;
    NLOHisto1DPtr _h_positronphoton_dR;
    NLOHisto1DPtr _h_muonphoton_dR;
    NLOHisto1DPtr _h_antimuonphoton_dR;
    NLOHisto1DPtr _h_muon_pT;
    NLOHisto1DPtr _h_muon_halved_pT;
    NLOHisto1DPtr _h_muon_pT_log;
    NLOHisto1DPtr _h_antimuon_pT;
    NLOHisto1DPtr _h_antimuon_halved_pT;
    NLOHisto1DPtr _h_antimuon_pT_log;
    NLOHisto1DPtr _h_electron_pT;
    NLOHisto1DPtr _h_electron_halved_pT;
    NLOHisto1DPtr _h_electron_pT_log;
    NLOHisto1DPtr _h_positron_pT;
    NLOHisto1DPtr _h_positron_halved_pT;
    NLOHisto1DPtr _h_positron_pT_log;
    NLOHisto1DPtr _h_muon_y;
    NLOHisto1DPtr _h_electron_y;
    NLOHisto1DPtr _h_antimuon_y;
    NLOHisto1DPtr _h_positron_y;
    NLOHisto1DPtr _h_muon_electron_pT;
    NLOHisto1DPtr _h_muon_electron_halved_pT;
    NLOHisto1DPtr _h_muon_electron_pT_log;
    NLOHisto1DPtr _h_muon_electron_m;
    NLOHisto1DPtr _h_muon_electron_halved_m;
    NLOHisto1DPtr _h_muon_electron_m_log;
    NLOHisto1DPtr _h_muon_electron_y;
    NLOHisto1DPtr _h_muon_electron_dR;
    NLOHisto1DPtr _h_muon_electron_dphi;
    NLOHisto1DPtr _h_muon_electron_costheta;
    NLOHisto1DPtr _h_muon_positron_pT;
    NLOHisto1DPtr _h_muon_positron_halved_pT;
    NLOHisto1DPtr _h_muon_positron_pT_log;
    NLOHisto1DPtr _h_muon_positron_m;
    NLOHisto1DPtr _h_muon_positron_halved_m;
    NLOHisto1DPtr _h_muon_positron_m_log;
    NLOHisto1DPtr _h_muon_positron_y;
    NLOHisto1DPtr _h_muon_positron_dR;
    NLOHisto1DPtr _h_muon_positron_dphi;
    NLOHisto1DPtr _h_muon_positron_costheta;

    NLOHisto1DPtr _h_electron_positron_pT;
    NLOHisto1DPtr _h_electron_positron_halved_pT;
    NLOHisto1DPtr _h_electron_positron_pT_log;
    NLOHisto1DPtr _h_electron_positron_m;
    NLOHisto1DPtr _h_electron_positron_halved_m;
    NLOHisto1DPtr _h_electron_positron_m_log;
    NLOHisto1DPtr _h_electron_positron_y;
    NLOHisto1DPtr _h_electron_positron_dR;
    NLOHisto1DPtr _h_electron_positron_dphi;
    NLOHisto1DPtr _h_electron_positron_costheta;

    NLOHisto1DPtr _h_muon_antimuon_pT;
    NLOHisto1DPtr _h_muon_antimuon_halved_pT;
    NLOHisto1DPtr _h_muon_antimuon_pT_log;
    NLOHisto1DPtr _h_muon_antimuon_m;
    NLOHisto1DPtr _h_muon_antimuon_halved_m;
    NLOHisto1DPtr _h_muon_antimuon_m_log;
    NLOHisto1DPtr _h_muon_antimuon_y;
    NLOHisto1DPtr _h_muon_antimuon_dR;
    NLOHisto1DPtr _h_muon_antimuon_dphi;
    NLOHisto1DPtr _h_muon_antimuon_costheta;

    NLOHisto1DPtr _h_antimuon_electron_pT;
    NLOHisto1DPtr _h_antimuon_electron_halved_pT;
    NLOHisto1DPtr _h_antimuon_electron_pT_log;
    NLOHisto1DPtr _h_antimuon_electron_m;
    NLOHisto1DPtr _h_antimuon_electron_halved_m;
    NLOHisto1DPtr _h_antimuon_electron_m_log;
    NLOHisto1DPtr _h_antimuon_electron_y;
    NLOHisto1DPtr _h_antimuon_electron_dR;
    NLOHisto1DPtr _h_antimuon_electron_dphi;
    NLOHisto1DPtr _h_antimuon_electron_costheta;
    NLOHisto1DPtr _h_antimuon_positron_pT;
    NLOHisto1DPtr _h_antimuon_positron_halved_pT;
    NLOHisto1DPtr _h_antimuon_positron_pT_log;
    NLOHisto1DPtr _h_antimuon_positron_m;
    NLOHisto1DPtr _h_antimuon_positron_halved_m;
    NLOHisto1DPtr _h_antimuon_positron_m_log;
    NLOHisto1DPtr _h_antimuon_positron_y;
    NLOHisto1DPtr _h_antimuon_positron_dR;
    NLOHisto1DPtr _h_antimuon_positron_dphi;
    NLOHisto1DPtr _h_antimuon_positron_costheta;


    NLOHisto1DPtr _h_4l_m;
    NLOHisto1DPtr _h_4l_halved_m;
    NLOHisto1DPtr _h_4l_m_low;
    NLOHisto1DPtr _h_4l_m_log;

    NLOHisto1DPtr _h_Zee_pT;
    NLOHisto1DPtr _h_Zee_pT_log;
    NLOHisto1DPtr _h_Zmm_pT;
    NLOHisto1DPtr _h_Zmm_pT_log;
    NLOHisto1DPtr _h_ZeeZmm_dphi;
    NLOHisto1DPtr _h_ZeeZmm_dy;
    NLOHisto1DPtr _h_ZeeZmm_dR;
    NLOHisto1DPtr _h_Zee_m;
    NLOHisto1DPtr _h_Zmm_m;
    NLOHisto1DPtr _h_4l_j_dphi;

    //@}

  };

    
  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(SS_ZZ_2e2mu_J30pT);
}
